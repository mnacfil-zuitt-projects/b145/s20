// blueprints

function Pokemon(name,level) {
	// properties
	this.name = name;
	this.level = level;
	this.attack = 2 * level;
	this.health = 4 * level
	// methods
	this.tackle = function(targetPokemon) {
		console.log(`${this.name} has tackled ${targetPokemon.name}`)
		console.log(targetPokemon.health - this.attack)
		targetPokemon.showHealth()
	}
	this.faint = function () {
		console.log(`${this.name} fainted`)
	}
	this.showHealth = function () {
		console.log('Health Point ' + this.health)
	}
}

const pikachu = new Pokemon('Pikachu', 16);
const ratata = new Pokemon('Ratata', 8);

pikachu.tackle(ratata)
